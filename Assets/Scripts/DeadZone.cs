﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class DeadZone : MonoBehaviourPun
{
    public List<GameObject> spawnPoint = new List<GameObject>();
    private int score = 0;
    private void Start()
    {
        foreach (GameObject spawn in GameObject.FindGameObjectsWithTag("SpawnPoint"))
        {
            spawnPoint.Add(spawn);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!photonView.IsMine)
            return;
        if (other.CompareTag("DeadZone"))
        {
            photonView.RPC("PlayerDead", photonView.Owner);
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable()
                    {{PunGameSetting.PLAYER_2_SCOREKEY, score++}});
            }
            else
            {
                PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable()
                    {{PunGameSetting.PLAYER_1_SCOREKEY, score++}});
            }
        }
    }

    [PunRPC]
    public void PlayerDead()
    {
        var randomSpawn = Random.Range(0, spawnPoint.Count);
        Vector2 direction = spawnPoint[randomSpawn].transform.position;
        this.transform.position = direction;
        Debug.Log("Respawned");
    }
}
