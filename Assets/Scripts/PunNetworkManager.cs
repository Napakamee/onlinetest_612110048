﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class PunNetworkManager : ConnectAndJoinRandom 
{
    public static PunNetworkManager singleton;

    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;
    
    public bool isGameStart = false;
    public bool isFirstSetting = false;
    public bool isGameOver = false;
    public GameObject[] itemPrefab;
    public int numberOfItem = 5;
    float m_count = 0;
    public float itemDropCount = 10;

    public GameObject[] itemSpawnPoint;
    
    public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;

    public delegate void FirstSetting();
    public static event FirstSetting OnFirstSetting;

    [Header("ScoreBoard")] 
    public Text p1Score;
    public Text p2Score;
    public Text p3Score;
    public Text p4Score;

    private void Awake()
    {
        singleton = this;
        OnPlayerSpawned += SpawnPlayer;
        OnFirstSetting += FirstRoomSetting;
    }

    
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        
        Debug.Log("New Player. " + newPlayer.ToString());
        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Camera.main.gameObject.SetActive(false);

        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }

        //PhotonNetwork.CurrentRoom.CustomProperties
        //PhotonNetwork.CurrentRoom.Players[0].CustomProperties
    }

    public void SpawnPlayer()
    {
        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            //PunNetworkManager.singleton.SpawnPlayer();
            // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
            PhotonNetwork.Instantiate(GamePlayerPrefab.name,
                new Vector3(0f, 5f, 0f), Quaternion.identity, 0);

            isGameStart = true;
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }
    }

    private void Update() {
        if(PhotonNetwork.IsMasterClient != true)
            return;
        if (isGameStart == true)
        {
            if (isFirstSetting == false)
                OnFirstSetting();
            if (isFirstSetting == true)
            {
                ItemDrop();
            }
            else ItemDrop();
        }
    }
    
    private void FirstRoomSetting()
    {
        isFirstSetting = true;
        Hashtable roomCustonProps = new Hashtable
        {
            {PunGameSetting.PLAYER_1_SCOREKEY,0},
            {PunGameSetting.PLAYER_2_SCOREKEY,0},
            {PunGameSetting.PLAYER_3_SCOREKEY,0},
            {PunGameSetting.PLAYER_4_SCOREKEY,0}
        };
        
        PhotonNetwork.CurrentRoom.SetCustomProperties(roomCustonProps);
        m_count = itemDropCount;
    }
    
    private void ItemDrop()
    {
        int spawnIndex = Random.Range(0, itemSpawnPoint.Length);
        int itemIbdex = Random.Range(0, itemPrefab.Length);
        if (GameObject.FindGameObjectsWithTag("Item").Length <
            numberOfItem)
        {
            m_count -= Time.deltaTime;

            if (m_count <= 0)
            {
                m_count = itemDropCount;
                PhotonNetwork.InstantiateRoomObject(itemPrefab[itemIbdex].name
                    , itemSpawnPoint[spawnIndex].transform.position
                    , itemSpawnPoint[spawnIndex].transform.rotation
                    , 0);
            }
        }

    }
    public override void OnDisconnected(DisconnectCause cause) {
        base.OnDisconnected(cause);

        if(Camera.main != null)
            Camera.main.gameObject.SetActive(true);
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        SetScoreText();
    }

    public void SetScoreText()
    {
        p1Score.text = "Player 1 Score: " +
                       PhotonNetwork.CurrentRoom.CustomProperties[PunGameSetting.PLAYER_1_SCOREKEY].ToString();
        p2Score.text = "Player 2 Score: " +
                       PhotonNetwork.CurrentRoom.CustomProperties[PunGameSetting.PLAYER_2_SCOREKEY].ToString();
        
    }
}
