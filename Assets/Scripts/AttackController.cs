﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.Serialization;

public class AttackController : MonoBehaviourPun, IPunObservable
{
    //[SerializeField] private Weapons weapon;
    [Header("Objects")] [Tooltip("Use in FollowCursor.")]
    public GameObject weaponHolder;
    Vector2 aimDirection;
    float aimAngle;
    private Vector2 mousePos;
    private Rigidbody2D rb;
    Camera cam;
    
    [Tooltip("Attack Mesh/Sprite.")] 
    public SpriteRenderer attackBox;
    [Tooltip("Attack Collider.")] 
    public BoxCollider2D attackCollider;

    [Space] 
    [SerializeField] private float waitTime;
    [SerializeField] private bool isAttack;
    public WeaponStat weaponStat;

    private float _hideAttackTime = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponentInChildren<Camera>();
        rb = GetComponent<Rigidbody2D>();
        attackCollider.enabled = false;
        attackBox.enabled = false;
        isAttack = false;
        waitTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.IsMine)
            return;

        TimeUpdate();
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        //Attack
        if (Input.GetMouseButtonDown(0))
        {
            Attack();
        }
    }

    //Collect Item
    private void OnTriggerStay2D(Collider2D other)
    {
        if (!photonView.IsMine)
            return;
        
        if (other.gameObject.CompareTag("Item") && Input.GetKeyDown(KeyCode.F))
        {
            Item item = other.GetComponent<Item>();
            Weapon newWeapon = item.weapon;
            weaponStat.weapon = newWeapon;
            Debug.Log("Collected " + newWeapon.weaponName);
            weaponStat.ChangeWeapon();
            Destroy(other.gameObject);
        }
    }

    private void TimeUpdate()
    {
        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
        }
        else
        {
            //isAttack = false; //bool for animation
            waitTime = 0;
        }

        //Hide Attack
        if (_hideAttackTime >= 0 && isAttack)
        {
            _hideAttackTime -= Time.deltaTime;
        }

        if (_hideAttackTime <= 0)
        {
            _hideAttackTime = 0.2f;
            attackCollider.enabled = false;
            attackBox.enabled = false;
            isAttack = false;
        }
    }

    private void Attack()
    {
        if (waitTime <= 0)
        {
            //isAttack = true;  //bool for animation
            WeaponFire();
            waitTime = weaponStat.atkSpd;
        }
    }

    private void WeaponFire()
    {
        if (!weaponStat.isRange)//Melee attack
        {
            attackCollider.enabled = true;
            attackBox.enabled = true;
        }
        else //Range attack
        {
            object[] data = {photonView.ViewID};
            GameObject bullet = PhotonNetwork.Instantiate(this.weaponStat.bullet.name,
                weaponHolder.transform.position + (weaponHolder.transform.up * 1.5f),
                weaponHolder.transform.rotation,
                0,
                data);
            
        }
        
        isAttack = true;
        
    }


    private void FollowCursor()
    {
        aimDirection = mousePos - rb.position;
        aimAngle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg - 90f;
        weaponHolder.transform.rotation = Quaternion.Euler(0,0, Mathf.Atan2(mousePos.y - transform.position.y, 
            mousePos.x - transform.position.x) * Mathf.Rad2Deg - 90f);
    }

    private void FixedUpdate()
    {
        if (!photonView.IsMine)
            return;
        FollowCursor();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(attackCollider.enabled);
            stream.SendNext(attackBox.enabled);
        }
        else
        {
            attackCollider.enabled = (bool) stream.ReceiveNext();
            attackBox.enabled = (bool) stream.ReceiveNext();
        }
    }
}